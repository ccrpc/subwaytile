# Subway Tile
Unobtrusive tile server for MBTiles.

## Usage
```
subwaytile [data_root] [tile_uris]
npm start -- [data_root] [tile_uris]
```
### Example
```
subwaytile /data https://a.tile.example.com https://b.tile.example.com
```

Given a tileset with the path `/data/research/landuse.mbtiles` and the above
configuration, the endpoints are:

```
# TileJSON
http://localhost:8080/research/landuse.json

# Tiles
https://a.tile.example.com/research/landuse/{z}/{x}/{y}.pbf
https://b.tile.example.com/research/landuse/{z}/{x}/{y}.pbf
```

## Credits
Subway Tile was developed by Matt Yoder for the
[Champaign County Regional Planning Commission][1].

## License
Subway Tile is available under the terms of the [BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/subwaytile/blob/master/LICENSE.md
