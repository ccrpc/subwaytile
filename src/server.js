#!/usr/bin/env node

const url = require('url');
const express = require('express');
const path = require('path');

const FontStack = require('./fonts.js');
const SubwayTile = require('./tiles.js');

if (process.argv.length < 3) {
  process.stdout.write('An MBTiles directory is required.\n\n');
  process.stdout.write('Usage: npm start /path/to/mbtiles\n');
}

const app = express();
const root = path.resolve(process.argv[2]);
const sTile = new SubwayTile(root);
const fStack = new FontStack(root);

if (process.env.SUBWAYTILE_CORS_ANY === 'true')
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });

app.get(/^\/(.+)\.json$/, async (req, res) => {
  let tileURIs = process.argv.slice(3);
  if (tileURIs.length === 0) {
    let proto = req.get('X-Forwarded-Proto') || req.protocol;
    let host = req.get('X-Forwarded-Host') || req.get('host');
    tileURIs.push([proto + '://' + host]);
  }

  let urlParts = url.parse(req.url);
  let {err, tilejson} = await sTile.getTileJSON(
    req.params['0'], tileURIs, urlParts.query);

  if (err === 'Invalid source') {
    return res.status(404).send(err);
  } else if (err) {
    return res.status(500).send(err);
  }

  res.send(tilejson);
});

app.get(/^\/(.+)\/(\d+)\/(\d+)\/(\d+)\.[a-z0-9]{3,4}$/, async (req, res) => {
  let {err, tile, headers} = await sTile.getTile(
    req.params['0'],
    parseInt(req.params['2']),
    parseInt(req.params['3']),
    parseInt(req.params['1']));

  if (err === 'Invalid source') {
    return res.status(404).send(err);
  } else if (err === 'Tile does not exist') {
    return res.status(204).send(err);
  } else if (err) {
    return res.status(500).send(err);
  }

  res.set(headers).send(tile);
});

app.get(/^\/(.+)\/(\d+-\d+)\.pbf$/, async (req, res) => {
  let pathString = req.params['0'];
  let range = req.params['1'];

  let parts = pathString.split('/');
  let faceString = parts.pop();
  let faces = faceString.split(',');
  let relPath = path.join(...parts);

  let {err, buffer} = await fStack.combine(faces, range, relPath);
  if (err === 'No matching faces') {
    return res.status(404).send(err);
  } else if (err) {
    return res.status(500).send(err);
  }

  return res.send(buffer);
});

app.listen(8080);
