const fs = require('fs');
const { promisify } = require('util');
const statAsync = promisify(fs.stat);
const path = require('path');
const MBTiles = require('@mapbox/mbtiles');


class SubwayTile {
  constructor(rootPath) {
    this.root = path.resolve(rootPath);
  }

  _load(uri) {
    return new Promise((resolve, reject) => {
      new MBTiles(`${uri}?mode=ro`, (err, mbtiles) =>
        (err) ? reject(err) : resolve(mbtiles));
    });
  }

  _getInfo(source) {
    return new Promise((resolve, reject) => {
      source.getInfo((err, data) => {
        source.close();
        (err) ? reject(err) : resolve(data);
      });
    });
  }

  _getTile(source, z, x, y) {
    return new Promise((resolve, reject) => {
      source.getTile(z, x, y, (err, tile, headers) => {
        source.close();
        (err) ? reject(err) : resolve({tile, headers});
      });
    });
  }

  async getSource(relPath) {
    let absPath = path.resolve(this.root, relPath + '.mbtiles');
    if (!absPath.startsWith(this.root)) return null;

    let stat;
    try {
      stat = await statAsync(absPath);
    } catch(e) {
      return null;
    }

    let sourceURI = 'mbtiles://' + absPath;

    let source;
    try {
      source = await this._load(sourceURI);
    } catch (e) {
      return null;
    }

    return source;
  }

  async getTileJSON(relPath, tileURIs, queryString) {
    let source = await this.getSource(relPath);
    if (!source) return {err: 'Invalid source'};

    let info;
    try {
      info = await this._getInfo(source);
    } catch(e) {
      return {err: 'Error retrieving metadata'};
    }

    let tilejson = {
      tilejson: '2.2.0',
      name: info.name,
      scheme: 'xyz',
      tiles: tileURIs.map(
        (uri) => uri + '/' + relPath + '/{z}/{x}/{y}.' + info.format +
          ((queryString) ? `?${queryString}` : ''))
    };

    if (info.bounds) tilejson.bounds = info.bounds;
    if (info.center) tilejson.center = info.center;
    if (info.minzoom) tilejson.minzoom = info.minzoom;
    if (info.maxzoom) tilejson.maxzoom = info.maxzoom;
    if (info.attribution) tilejson.attribution = info.attribution;
    if (info.description) tilejson.description = info.description;
    if (info.version) tilejson.version = info.version;
    if (info.vector_layers) tilejson.vector_layers = info.vector_layers;

    return {tilejson};
  }

  async getTile(relPath, x, y, z) {
    let source = await this.getSource(relPath);
    if (!source) return {err: 'Invalid source'};

    let tileinfo;
    try {
      tileinfo = await this._getTile(source, z, x, y);
    } catch(e) {
      if (/Tile does not exist/.test(e)) return {err: 'Tile does not exist'};
      return {err: 'Error retrieving tile'};
    }

    return tileinfo;
  }
}

module.exports = SubwayTile;
