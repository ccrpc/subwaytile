const composite = require('@mapbox/glyph-pbf-composite');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const readFileAsync = promisify(fs.readFile);


class FontStack {
  constructor(rootPath) {
    this.root = path.resolve(rootPath);
  }

  async _load(face, range, relPath) {
    let absPath = path.resolve(this.root, relPath, face, range + '.pbf');
    try {
      return await readFileAsync(absPath);
    } catch (e) {
      if (e.code === 'ENOENT') return null;
      throw(e);
    }
  }

  async combine(faces, range, relPath) {
    let buffers = await Promise.all(
      faces.map((face) => this._load(face, range, relPath)));
    buffers = buffers.filter((buffer) => buffer !== null);

    if (buffers.length === 0) return {err: 'No matching faces'};
    if (buffers.length === 1) return {buffer: buffers[0]};

    try {
      return {buffer: composite.combine(buffers)};
    } catch(e) {
      return {err: e.message}
    }
  }
}

module.exports = FontStack;
