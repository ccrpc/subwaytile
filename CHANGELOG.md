# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.3.1 (2021-03-18)


### Bug Fixes

* explicitly close the MBTiles source after each use ([a4d47b6](https://gitlab.com/ccrpc/subwaytile/commit/a4d47b65a0fa77e8a1be5bcf81cbb5b54adb4332))
* remove source caching ([a75337f](https://gitlab.com/ccrpc/subwaytile/commit/a75337fe434d7f96ee6e64b33940b261b25c7df7))

## [0.3.0] - 2019-06-04
- Pass through query string to tiles URL.
- Clean source cache periodically to remove stale entries.
- Updated dependencies.

## [0.2.0] - 2019-04-22
- Implemented font compositing for SDF glyphs.

## [0.1.1] - 2019-04-18
- Fixed check for CORS environment variable.

## [0.1.0] - 2019-04-18
- Initial release
